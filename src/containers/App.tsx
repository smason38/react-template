import * as React from 'react'

interface PropTypes {

}

interface State {

}

export default class App extends React.Component<PropTypes, State> {

    render(): JSX.Element {
        return (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ width: '25%', background: 'red' }}>
                </div>

                <div style={{ width: '50%', opacity: 0, pointerEvents: 'none' }}>
                </div>

                <div style={{ width: '25%', background: 'blue' }}>
                </div>
            </div>
        );
    }
}