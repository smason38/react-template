
export class vec2 {
    public static zero: vec2 = new vec2(0, 0); 

    public x: number;
    public y: number;

    constructor(x?: number, y?: number) {
        if (x) {
            this.x = x;
        }
        if (y) {
            this.y = y;
        }
    }

    public dot(other: vec2): number {
        return this.x * other.x + this.y * other.y;
    }

    public normalize(): vec2 {
        let a = Math.abs(Math.sqrt((this.x * this.x) + (this.y * this.y)));
        return new vec2(this.x / a, this.y / a);
    }

    public add(other: vec2): vec2 {
        return new vec2(this.x + other.x, this.y + other.y);
    }

    public addScalar(scalar: number): vec2 {
        return new vec2(this.x + scalar, this.y + scalar);
    }

    public sub(other: vec2): vec2 {
        return new vec2(this.x - other.x, this.y - other.y);
    }

    public subScalar(scalar: number): vec2 {
        return new vec2(this.x - scalar, this.y - scalar);
    }

    public mult(other: vec2): vec2 {
        return new vec2(this.x * other.x, this.y * other.y);
    }

    public multScalar(scalar: number): vec2 {
        return new vec2(this.x * scalar, this.y * scalar);
    }

    public div(other: vec2): vec2 {
        return new vec2(this.x / other.x, this.y / other.y);
    }

    public divScalar(scalar: number): vec2 {
        return new vec2(this.x / scalar, this.y / scalar);
    }
}