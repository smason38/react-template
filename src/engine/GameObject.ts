export interface GameObject {
    update(totalTime: number, delta_t: number): void;
    draw(ctx: CanvasRenderingContext2D, totalTime: number, delta_t: number): void;
}