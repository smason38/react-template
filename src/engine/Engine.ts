import { Board } from '../ballz/Board';

export const canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement;
const context: CanvasRenderingContext2D = canvas.getContext('2d');

let board: Board;

function handleResize() {
    let w = window.innerWidth;
    let h = window.innerHeight;
    canvas.width = w;
    canvas.height = h;
}

function initialize() {
    handleResize();
    board = new Board(7, 7);
}

window.onresize = () => {
    handleResize();
};

window.addEventListener('load', initialize);

const update = (totalTime: number, delta_t: number) => {
    board.update(totalTime, delta_t);
};

const draw = (ctx: CanvasRenderingContext2D, totalTime: number, delta_t: number) => {
    // clear to black each frame
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    board.draw(ctx, totalTime, delta_t);

    //players.forEach(player => player.draw(ctx))

    // ctx.fillStyle = 'red';
    // drawDots();
};

let t = 4294967296.0; // start at 2^32 seconds and precision remains constant for 136 years
let second = 0.0;
let dt = 0.0166666666667; // 16 ms

let currentTime = performance.now() * 0.001;
let accumulator = 0.0;

export const gameLoop = () => {
    requestAnimationFrame(gameLoop);
    let newTime = performance.now() * 0.001;
    let frameTime = newTime - currentTime;
    currentTime = newTime;

    accumulator += frameTime;

    while (accumulator >= dt) {
        update(t, dt);

        accumulator -= dt;
        t += dt;
        second += dt;
    }

    draw(context, t, frameTime);

    if (second >= 1.0) {
        //console.log("frame");
        second = 0.0;
    }
};