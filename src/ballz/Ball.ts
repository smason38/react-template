import { GameObject } from 'engine/GameObject';
import { vec2 } from 'engine/Vec2';
import { Board } from '../ballz/Board';
import { canvas } from 'engine/Engine';

export enum BallState {
    PRELAUNCH,
    LAUNCHED,
    DEAD
}

class State {
    public p: vec2;
    public v: vec2;
}

class Derivative {
    public dp: vec2;
    public dv: vec2;
}

export class Ball implements GameObject {
    private state:      State;
    private derivative: Derivative;

    private r:          number;
    private ballState:  BallState;

    private board: Board;

    constructor(board: Board) {
        this.board = board;
    }

    private evaluate(initial: State, t: number, dt: number, d: Derivative): Derivative {
        let state = new State();
        state.p = initial.p.add(d.dp.multScalar(dt)); // p + dp * dt
        state.v = initial.v.add(d.dv.multScalar(dt)); // v + dv * dt

        let output = new Derivative();
        output.dp = state.v;
        output.dv = this.acceleration(state, t + dt);
        return output;
    }

    private acceleration(state: State, t: number): vec2 {
        return vec2.zero;
    }

    private integrate(t: number, dt: number) {
        let a: Derivative, b: Derivative, c: Derivative, d: Derivative;
        a = this.evaluate(this.state, t, 0.0, new Derivative());
        b = this.evaluate(this.state, t, dt * 0.5, a);
        c = this.evaluate(this.state, t, dt * 0.5, b);
        d = this.evaluate(this.state, t, dt, c);

        let dpdt = ((b.dp.add(c.dp)).multScalar(2.0).add(a.dp).add(d.dp)).multScalar(1.0 / 6.0);
        let dvdt = ((b.dv.add(c.dv)).multScalar(2.0).add(a.dv).add(d.dv)).multScalar(1.0 / 6.0);

        this.state.p = this.state.p.add(dpdt.multScalar(dt));
        this.state.v = this.state.v.add(dvdt.multScalar(dt));
    }

    update(totalTime: number, delta_t: number) {
        this.integrate(totalTime, delta_t);
    }

    draw(ctx: CanvasRenderingContext2D, totalTime: number, delta_t: number) {
        let ox = canvas.width * 0.25;
        let oy = canvas.height * 0.8;
        let radius = canvas.width * 0.5 * (1 / 40);

        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.arc(ox + this.p.x, oy - this.p.y, radius, 0, 2 * Math.PI);
        ctx.fill();
    }
}