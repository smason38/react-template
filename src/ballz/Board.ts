import { GameObject } from 'engine/GameObject';
import { Block } from '../ballz/Block';
import { canvas } from 'engine/Engine';

export class Board implements GameObject {
    private xSize: number;
    private ySize: number;

    private blocks: Block[][];

    constructor(size_x: number, size_y: number) {
        this.xSize = size_x;
        this.ySize = size_y;

        this.blocks = [];

        for (let x = 0; x < this.xSize; x++) {
            this.blocks[x] = [];
            for (let y = 0; y < this.ySize; y++) {
                this.blocks[x][y] = new Block(this, x, y, Math.floor(Math.random() * 64));
            }
        }
    }

    getBoardSizeX(): number {
        return canvas.width * 0.5;
    }

    getBoardSizeY(): number {
        return canvas.height * 0.8;
    }

    get numBlocksX(): number {
        return this.xSize;
    }

    get numBlocksY(): number {
        return this.ySize;
    }

    update(totalTime: number, delta_t: number) {
        for (let x = 0; x < this.xSize; x++) {
            for (let y = 0; y < this.ySize; y++) {
                let block = this.blocks[x][y];
                if (block) {
                    block.update(totalTime, delta_t);
                }
            }
        }
    }

    draw(ctx: CanvasRenderingContext2D, totalTime: number, delta_t: number) {
        for (let x = 0; x < this.xSize; x++) {
            for (let y = 0; y < this.ySize; y++) {
                let block = this.blocks[x][y];
                if (block) {
                    block.draw(ctx, totalTime, delta_t);
                }
            }
        }
    }


}