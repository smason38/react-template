import { GameObject } from 'engine/GameObject';
import { Board } from '../ballz/Board';

export enum BlockState {
    Ready,
    Transitioning
}

let grad = [
    '#2491FF',
    '#2487FF',
    '#257CFF',
    '#2672FF',
    '#2767FF',
    '#285DFF',
    '#2953FF',
    '#2A49FF',
    '#2B3FFF',
    '#2C35FF',
    '#302DFF',
    '#3B2EFF',
    '#472FFF',
    '#5230FF',
    '#5E31FF',
    '#6932FF',
    '#7433FF',
    '#7F34FF',
    '#8A35FF',
    '#9536FF',
    '#A037FF',
    '#AA38FF',
    '#B539FF',
    '#BF3AFF',
    '#CA3BFF',
    '#D43CFF',
    '#DE3DFF',
    '#E83EFF',
    '#F23FFF',
    '#FC40FF',
    '#FF41F7',
    '#FF42ED',
    '#FF42E4',
    '#FF43DA',
    '#FF44D1',
    '#FF45C8',
    '#FF46BF',
    '#FF47B6',
    '#FF48AD',
    '#FF49A4',
    '#FF4A9B',
    '#FF4B92',
    '#FF4C8A',
    '#FF4D81',
    '#FF4E79',
    '#FF4F70',
    '#FF5068',
    '#FF5160',
    '#FF5258',
    '#FF5653',
    '#FF5F54',
    '#FF6955',
    '#FF7256',
    '#FF7C57',
    '#FF8558',
    '#FF8E59',
    '#FF975A',
    '#FFA05B',
    '#FFA95C',
    '#FFB25D',
    '#FFBB5E',
    '#FFC45F',
    '#FFCC60',
    '#FFD461'
];

export class Block implements GameObject {

    private board: Board;
    private value: number;
    private x: number;
    private y: number;
    private state: BlockState;

    constructor(board: Board, x: number, y: number, initialValue: number) {
        this.board = board;
        this.value = initialValue;
        this.x = x;
        this.y = y;
        this.state = BlockState.Ready;
    }

    update(totalTime: number, delta_t: number) {

    }

    draw(ctx: CanvasRenderingContext2D, totalTime: number, delta_t: number) {
        let blockWidth = this.board.getBoardSizeX() / this.board.numBlocksX;
        let blockHeight = this.board.getBoardSizeY() / this.board.numBlocksY;

        let left = this.board.getBoardSizeX() * 0.5;

        ctx.fillStyle = grad[this.value % 64];
        ctx.strokeStyle = 'none';
        ctx.fillRect(left + this.x * blockWidth + 2.5, this.y * blockHeight + 2.5, blockWidth - 5, blockHeight - 5);
        ctx.strokeStyle = 'black';
        ctx.font = '18px arial';
        ctx.textBaseline = 'hanging';
        ctx.fillStyle = 'black';
        let metrics = ctx.measureText(this.value.toString());
        ctx.fillText(this.value.toString(), left + this.x * blockWidth + (blockWidth * 0.5) - (metrics.width * 0.5), this.y * blockHeight + (blockHeight * 0.5) - 9);
    }
}